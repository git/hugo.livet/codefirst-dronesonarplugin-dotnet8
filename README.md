# codefirst-dronesonarplugin-dotnet8

Cette image Docker est fournie pour être utilisé sur Codefirst et inclut les outils nécessaires pour intégrer l'analyse et la génération de rapports SonarQube dans les projets .NET 8 en utilisant Drone CI.

## Détails de l'image

- Image de base : `mcr.microsoft.com/dotnet/sdk:8.0-alpine`
- Outils supplémentaires :
  - OpenJDK 17 (JRE)
  - Node.js
  - dotnet-sonarscanner
  - dotnet-reportgenerator-globaltool

## Utilisation

Pour utiliser cette image dans votre pipeline Drone CI, vous pouvez la référencer dans votre fichier `.drone.yml` :

```yaml
- name: code-analysis
      image: hub.codefirst.iut.uca.fr/hugo.livet/codefirst-dronesonarplugin-dotnet8
      commands:
        - cd votre_path/
        - dotnet restore Votre_sln.sln
        - dotnet sonarscanner begin /k:VotreKey /d:sonar.host.url=$${PLUGIN_SONAR_HOST} /d:sonar.coverageReportPaths="coveragereport/SonarQube.xml" /d:sonar.coverage.exclusions="Tests/**" /d:sonar.login=$${PLUGIN_SONAR_TOKEN} 
        - dotnet build Votre_sln.sln -c Release --no-restore
        - dotnet test Votre_sln.sln --logger trx --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura --collect "XPlat Code Coverage"
        - reportgenerator -reports:"**/coverage.cobertura.xml" -reporttypes:SonarQube -targetdir:"coveragereport"
        - dotnet publish Votre_sln.sln -c Release --no-restore -o CI_PROJECT_DIR/build/release
        - dotnet sonarscanner end /d:sonar.login=$${PLUGIN_SONAR_TOKEN}
      secrets: [ SECRET_SONAR_LOGIN ]
      settings:
        sonar_host: https://codefirst.iut.uca.fr/sonar/
        sonar_token:
          from_secret: SECRET_SONAR_LOGIN
      depends_on: [tests]
